#  ____                  
# |  _ \ _   _ _ __  ___ 
# | |_) | | | | '_ \/ __|
# |  _ <| |_| | | | \__ \
# |_| \_\\__,_|_| |_|___/
#                        

"""
    createrun(mlf::MLFlow, expid, name)

DOCSTRING

# Arguments:
- `mlf`: MLFlow struct containing the URI to the MLFlow server.
- `expid`: The unique ID generated when creating a new experiment.
- `name`: The name you want to give your experiment.
"""
function createrun(mlf::MLFlow, expid, name)
    endpoint = "runs/create"
    uri = joinpath(mlf.uri, endpoint)
    header = Dict("Content-Type" => "application/json")
    params = Dict("experiment_id" => expid, "run_name" => name, "start_time" => timestamp())
    params = JSON.json(params)
    r = HTTP.request("POST", uri, header, params)
    retval = JSON.parse(String(r.body))
    retval["run"]
end

"""
    updaterun(mlf::MLFlow, runid, status)

Update the status of a current run.

# Arguments:
- `mlf`: MLFlow struct containing the URI to the MLFlow server.
- `runid`: The unique run ID that was generated when you created the run you want to target.
- `status`: See the official [MLFlow API](https://www.mlflow.org/docs/latest/rest-api.html#runstatus) documentation for the different statuses.
"""
function updaterun(mlf::MLFlow, runid, status)
    endpoint = "runs/update"
    uri = joinpath(mlf.uri, endpoint)
    header = Dict("Content-Type" => "application/json")
    params = Dict("run_id" => runid, "status" => status, "end_time" => timestamp())
    params = JSON.json(params)
    r = HTTP.request("POST", uri, header, params)
    retval = JSON.parse(String(r.body))
    retval["run_info"]
end

"""
    delrun(mlf::MLFlow, runid)

Delete the run identified by the `runid`.
"""
function delrun(mlf::MLFlow, runid)
    endpoint = "runs/delete"
    uri = joinpath(mlf.uri, endpoint)
    header = Dict("Content-Type" => "application/json")
    params = Dict("run_id" => runid)
    params = JSON.json(params)
    try
        _ = HTTP.request("POST", uri, header, params)
        println("Run with ID=$(runid) successfully deleted")
    catch
        @warn "Couldn't find the run with ID=$(runid)"
    end
end

"""
    logmetric(mlf::MLFlow, runid, key, value, step)

Log a metric like loss or accuracy to the given run identified by `runid`.

# Arguments:
- `mlf`: MLFlow struct containing the URI to the MLFlow server.
- `runid`: The unique run ID that was generated when you created the run you want to target.
- `key`: The key you want to use for this metric, e.g., "Accuracy".
- `value`: The value of the metric which in the case of Accuracy could be 0.8.
- `step`: In Machine Learning this is usually the epoch that you would log as the step.
"""
function logmetric(mlf::MLFlow, runid, key, value, step)
    endpoint = "runs/log-metric"
    uri = joinpath(mlf.uri, endpoint)
    header = Dict("Content-Type" => "application/json")
    params = Dict("run_id" => runid, "key" => key, "value" => value, "timestamp" => timestamp(), "step" => step)
    params = JSON.json(params)
    r = HTTP.request("POST", uri, header, params)
    r.status == 200
end

"""
    logparam(mlf::MLFlow, runid, key, value)

DOCSTRING

# Arguments:
- `mlf`: MLFlow struct containing the URI to the MLFlow server.
- `runid`: The unique run ID that was generated when you created the run you want to target.
- `key`: Hyperparameter key like number of layers which could be "NumHidden".
- `value`: The value of the hyperparameter like 3 if you have 3 hidden layers in your network.
"""
function logparam(mlf::MLFlow, runid, key, value)
    endpoint = "runs/log-parameter"
    uri = joinpath(mlf.uri, endpoint)
    header = Dict("Content-Type" => "application/json")
    params = Dict("run_id" => runid, "key" => key, "value" => value)
    params = JSON.json(params)
    r = HTTP.request("POST", uri, header, params)
    r.status == 200
end


