# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/doktormike/mlflowrest/compare/v1.0.0...v1.0.1) (2024-06-12)


### Documentation

* Documented the runs interface. ([814b7b0](https://gitlab.com/doktormike/mlflowrest/commit/814b7b0a4765fe84c110556652b279a0e5682ac8))
* Examples of how to use it and disclaimers added. ([1764aac](https://gitlab.com/doktormike/mlflowrest/commit/1764aacbeb1c27d498c68bdf4df7825c5bd3b03c))

## 1.0.0 (2024-06-12)


### Features

* Created furst version of MLFlowREST. ([860dd80](https://gitlab.com/doktormike/mlflowrest/commit/860dd80f51ca6c2e55d2d349ec88ca25baf63360))


### Bug Fixes

* bug in documentation make. ([f9a13e0](https://gitlab.com/doktormike/mlflowrest/commit/f9a13e01a8d585418dab24ef3a0e6769f30577a5))
* fixed merge conflict. ([ba42bea](https://gitlab.com/doktormike/mlflowrest/commit/ba42bea0b45dfed45c5d595a3dabab80c391abbd))
* remove 1.6 testing. ([980b307](https://gitlab.com/doktormike/mlflowrest/commit/980b3074f93144d961cdd4c7832ab7083845b9f4))


### Documentation

* corrected links. ([7e3773f](https://gitlab.com/doktormike/mlflowrest/commit/7e3773f172f4ad3eb8f5b7ccaf98db2a8f3fe2bd))
* explanation for why I made it. ([2da85ac](https://gitlab.com/doktormike/mlflowrest/commit/2da85ac1634d970a4a9e76cea984cb2148db4531))
* fixed documentation. ([3cc11e6](https://gitlab.com/doktormike/mlflowrest/commit/3cc11e62ccfd0a191ebe85d6a68d76cb58b1015c))
* I cannot seem to get this right. ([2e21fe6](https://gitlab.com/doktormike/mlflowrest/commit/2e21fe606659c8534e5a8ec235cf88ea9d87674a))
* pages stupidity. ([199720c](https://gitlab.com/doktormike/mlflowrest/commit/199720c1e4b431c762cc60abce40cdb43446c9f0))
* set up pages properly. ([25c5fb6](https://gitlab.com/doktormike/mlflowrest/commit/25c5fb6a4e3cb1f989cd01d01e258641d3c92385))
