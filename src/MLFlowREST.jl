module MLFlowREST

# Write your package code here.

# Logger helper functions
using HTTP
using JSON

struct MLFlow
    uri
    MLFlow(uri) = new(joinpath(uri, "api/2.0/mlflow/"))
end

include("utils.jl")
include("experiments.jl")
include("runs.jl")

export MLFlow, createexperiment, getexperiment, delexperiment
export createrun, updaterun, delrun, logmetric, logparam

# Create new experiment and run from scratch and log some metrics and then delete the run and the experiment
# experiment_id = createexperiment(mlf, "aichemy-adme")
# run = createrun(mlf, experiment_id, "coolrunnings2")
# logmetric(mlf, run["info"]["run_id"], "rmse", 0.45, 0)
# logmetric(mlf, run["info"]["run_id"], "rmse", 0.35, 1)
# logmetric(mlf, run["info"]["run_id"], "rmse", 0.32, 2)
# logmetric(mlf, run["info"]["run_id"], "rmse", 0.25, 3)
# logmetric(mlf, run["info"]["run_id"], "rmse", 0.40, 4)
# run["info"] = updaterun(mlf, run["info"]["run_id"], "FINISHED")
# logparam(mlf, run["info"]["run_id"], "model-class", "dummy")
# delrun(mlf, run["info"]["run_id"])
# delexperiment(mlf, experiment_id)


end
