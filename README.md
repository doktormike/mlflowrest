# MLFlowREST

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://DoktorMike.gitlab.io/MLFlowREST/dev)
[![pipeline status](https://gitlab.com/doktormike/mlflowrest/badges/main/pipeline.svg)](https://gitlab.com/doktormike/mlflowrest/-/commits/main)
[![coverage report](https://gitlab.com/doktormike/mlflowrest/badges/main/coverage.svg)](https://gitlab.com/doktormike/mlflowrest/-/commits/main)
[![Latest Release](https://gitlab.com/doktormike/mlflowrest/-/badges/release.svg)](https://gitlab.com/doktormike/mlflowrest/-/releases)

## Introduction

An MLFlow REST API client for Julia. I wrote this because I needed a lightweight library that could communicate with a remote MLFlow server using an API.

## Installation

```julia
using Pkg

Pkg.add("https://gitlab.com/doktormike/mlflowrest.git")
```

## Quick start

If you just want to run a local MLFlow server I recommend doing it through Docker.

```bash
docker run -d -v ./mlruns:/mlruns -p 5000:5000 --name mlflow ghcr.io/mlflow/mlflow:latest mlflow ui --host 0.0.0.0
```

This creates a local `mlruns` folder where you're starting it from and then you can use this package to log your experiments.

```julia
using Base: @kwdef
using MLFlowREST

@kwdef mutable struct Config
        dropout::Bool = true
        optimizer::String = "AdamW"
        lossfn::String = "MaxAE"
        batchsize::Int = 64
        epochs::Int = 500
end

# Make logger
mlf = MLFlow("http://localhost:5000")

# Create new experiment and run from scratch and log some metrics and then delete the run and the experiment
experiment_id = createexperiment(mlf, "aal-genie")
#experiment = getexperiment(mlf, "aal-genie")
#experiment_id = experiment["experiment_id"]

# Create a run
runname = "cool-runnings"
mlrun = createrun(mlf, experiment_id, runname)
exprun = mlrun["info"]["run_id"]

# Log hyper parameters
logparam(mlf, exprun, "dropout", string(config.dropout))
logparam(mlf, exprun, "optimizer", string(config.optimizer))
logparam(mlf, exprun, "lossfn", string(config.loss))
logparam(mlf, exprun, "batchsize", string(config.batchsize))
logparam(mlf, exprun, "epochs", string(config.epochs))

# Log metrics
logmetric(mlf, exprun, "trnloss", 0.32, 0)
logmetric(mlf, exprun, "valloss", 0.71, 0)


# complete the experiment
updaterun(mlf, exprun, "FINISHED")
```

## Disclaimer

This is by no means a complete implementation of the MLFlow REST API but it served my purpose. Feel free to contribute and improve on it. 
