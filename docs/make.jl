using MLFlowREST
using Documenter

DocMeta.setdocmeta!(MLFlowREST, :DocTestSetup, :(using MLFlowREST); recursive=true)

makedocs(;
    modules=[MLFlowREST],
    authors="Michael Green",
    sitename="MLFlowREST",
    repo = Documenter.Remotes.GitLab("DoktorMike", "MLFlowREST"),
    format=Documenter.HTML(;
        canonical="https://DoktorMike.gitlab.io/MLFlowREST",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
