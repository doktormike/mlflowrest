#  _____                      _                      _   
# | ____|_  ___ __   ___ _ __(_)_ __ ___   ___ _ __ | |_ 
# |  _| \ \/ / '_ \ / _ \ '__| | '_ ` _ \ / _ \ '_ \| __|
# | |___ >  <| |_) |  __/ |  | | | | | | |  __/ | | | |_ 
# |_____/_/\_\ .__/ \___|_|  |_|_| |_| |_|\___|_| |_|\__|
#            |_|                                        

"""
    createexperiment(mlf::MLFlow, name)

Create an experiment and name it.
"""
function createexperiment(mlf::MLFlow, name)
    endpoint = "experiments/create"
    uri = joinpath(mlf.uri, endpoint)
    header = Dict("Content-Type" => "application/json")
    params = JSON.json(Dict("name" => name))
    r = HTTP.request("POST", uri, header, params)
    retval = JSON.parse(String(r.body))
    retval["experiment_id"]
end

"""
    getexperiment(mlf::MLFlow, name)

Fetch and existing experiment by name which should be given as a string.
"""
function getexperiment(mlf::MLFlow, name)
    #r = HTTP.request("GET", "http://localhost:5000/api/2.0/mlflow/experiments/get-by-name"; query=["experiment_name" => "aichemy-adme"], verbose=2)
    endpoint = "experiments/get-by-name"
    uri = joinpath(mlf.uri, endpoint)
    r = HTTP.request("GET", uri; query=["experiment_name" => name], verbose=0) # Set to > 0 for verbosity
    retval = JSON.parse(String(r.body))
    retval["experiment"]
end

"""
    delexperiment(mlf::MLFlow, id)

Delete an existing experiment by using it's unique ID.
"""
function delexperiment(mlf::MLFlow, id)
    endpoint = "experiments/delete"
    uri = joinpath(mlf.uri, endpoint)
    header = Dict("Content-Type" => "application/json")
    params = JSON.json(Dict("experiment_id" => id))
    try
        _ = HTTP.request("POST", uri, header, params)
        println("Experiment with ID=$(id) successfully deleted")
    catch
        @warn "Couldn't find the experiment with ID"
    end
end



